package clases;

import java.util.Iterator;
import java.util.NoSuchElementException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;

/**
 *
 * @author Ing. Valerio Frittelli.
 */
public class TSBQueueUniqueValuesTest
{
    private TSBQueueUniqueValues<Integer> instance;
    private TSBQueueUniqueValues<Integer> empty;

    public TSBQueueUniqueValuesTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass() 
    {
    }
    
    @AfterClass
    public static void tearDownClass() 
    {
    }
    
    @Before
    public void setUp() 
    {
        System.out.println("* UtilsJUnit4Test: @Before method");
        int data[] = {1, 5, 7, 6, 10, 2, 8, 11};
        
        empty = new TSBQueueUniqueValues<>();
        
        instance = new TSBQueueUniqueValues<>();
        for(int value : data)
        {
            instance.add(value);
        }
    }
    
    @After
    public void tearDown() 
    {
    }
    
    /**
     * Test of add method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test ()
    public void testAdd() 
    {
        System.out.println("add");
        assertTrue(instance.add(3));
        assertFalse(instance.add(10));

        int data[] = {1, 5, 7, 6, 10, 2, 8, 11, 3};
        Iterator it = instance.iterator();
        for(int i=0; i<data.length; i++)
        {
            assertTrue(data[i] == (Integer)it.next());
        }

        instance.add(null);
    }

    /**
     * Test of iterator method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test (expected = UnsupportedOperationException.class)
    public void testIterator() 
    {
        System.out.println("iterator");
        Iterator r1 = instance.iterator();
        assertNotEquals(null, r1);        
        assertTrue(r1.hasNext());
        assertNotEquals(null, r1.next());
        r1.remove();
    }

    /**
     * Test of size method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test
    public void testSize() 
    {
        System.out.println("size");
        assertEquals(8, instance.size());
        assertEquals(0, empty.size());
    }

    /**
     * Test of offer method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test (expected = NullPointerException.class)
    public void testOffer() 
    {
        System.out.println("offer");
        assertTrue(instance.offer(3));
        assertFalse(instance.offer(10));
        instance.add(null);
    }

    /**
     * Test of remove method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test (expected = NoSuchElementException.class)
    public void testRemove() 
    {
        System.out.println("remove");
        int ta = instance.size();
        assertEquals((Integer)1, instance.remove());
        assertEquals(ta - 1, instance.size());
        int r1 = empty.remove();
    }

    /**
     * Test of poll method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test
    public void testPoll() 
    {
        System.out.println("poll");
        int ta = instance.size();
        assertEquals((Integer)1, instance.poll());
        assertEquals(ta - 1, instance.size());
        assertNull(empty.poll());
    }

    /**
     * Test of element method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test (expected = NoSuchElementException.class)
    public void testElement() 
    {
        System.out.println("element");
        assertEquals((Integer)1, instance.element());
        int r1 = empty.element();
    }

    /**
     * Test of peek method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test
    public void testPeek() 
    {
        System.out.println("peek");
        assertEquals((Integer)1, instance.peek());
        assertNull(empty.peek());
    }

    /**
     * Test of clone method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test
    public void testClone() 
    {
        System.out.println("clone");
        try
        {
            TSBQueueUniqueValues<Integer> copy = (TSBQueueUniqueValues<Integer>)instance.clone();
            assertTrue(instance.equals(copy));
        }
        catch(CloneNotSupportedException e)
        {
        }
    }
    
    /**
     * Test of equals method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test
    public void testEquals() 
    {
        System.out.println("equals");
        assertTrue(instance.equals(instance));
        assertFalse(instance.equals(empty));
    }
    
    /**
     * Test of hashCode method, of class TSBQueueUniqueValues.
     */
    @org.junit.Test
    public void testHashCode()
    {
        System.out.println("hashCode");
        assertNotEquals(empty.hashCode(), instance.hashCode());
    }

}

package clases;

import java.util.Iterator;

public class Main {
    public static void main(String args[]){
        TSBQueueUniqueValues<Integer> colaPrueba = new TSBQueueUniqueValues<Integer>();
        int data[] = {1,5,7,6,10,55,77,8,10,11,15,20,25};
        for(int value : data)
        {
            colaPrueba.add(value);
        }
        int poselem1 = colaPrueba.exists(55);
        int poselem2 = colaPrueba.exists(44);
        //Las posiciones las muestro considerando que empiezan en 0.
        System.out.println("Posición elemento 1: "+poselem1);
        System.out.println("Posición elemento 2: "+poselem2);
        System.out.println("Cola sin modificar: "+colaPrueba);
        colaPrueba.swapEnds();
        System.out.println("Cola con los extremos intercambiados: "+colaPrueba);
        Iterator rts =colaPrueba.reverseIndexIterator();
        while (rts.hasNext()){

            System.out.println("Elemento: "+rts.next());}
        System.out.println("Cola: "+colaPrueba);

    }
}

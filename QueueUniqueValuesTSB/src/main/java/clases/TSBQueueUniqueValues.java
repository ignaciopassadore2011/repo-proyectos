package clases;

import java.io.Serializable;
import java.util.*;

public class TSBQueueUniqueValues<E> extends AbstractQueue<E> implements Serializable, Cloneable
{
    private  int fondo, capacidad;
    private  Object cola[];
    public  TSBQueueUniqueValues(){
        this.fondo  = 0;
        capacidad= 10;
        cola = new Object[capacidad];
    }
    public TSBQueueUniqueValues(Collection<? extends E> c){
        this.cola = c.toArray();
        capacidad = c.size();
        fondo = c.size();
    }
    public TSBQueueUniqueValues(int capacidadinicial){
        if (capacidadinicial <= 0)
        {
            capacidadinicial = 10;
        }
        cola = new Object[capacidadinicial];
        capacidad = capacidadinicial;
        fondo = 0;
    }
    public void asegurarCapacidad(int capacidadMin)
    {
        if(capacidadMin == capacidad) return;
        if(capacidadMin < fondo) return;

        Object[] temp = new Object[capacidadMin];
        System.arraycopy(cola, 0, temp, 0, fondo);
        cola = temp;
        capacidad = capacidadMin;
    }
    @Override
    public boolean contains(Object e)
    {
        if(e == null) return true;

        for(int i=0; i<fondo; i++)
        {
            if(e.equals(cola[i]) && cola[i]!=null) return  true;
        }
        return false;
    }
    @Override
    public boolean add(E e)
    {
        if (contains(e)) return false;
        if (fondo+1 == capacidad) this.asegurarCapacidad(capacidad*2);
        if (fondo==0){
        cola[fondo]= e;
            fondo++;}
        else{ cola[fondo]= e;
            fondo++;}
        return true;
    }
    public int exists(E e){
        if(e == null) throw new NullPointerException();

        for(int i=0; i<fondo; i++)
        {
            if(e.equals(cola[i]) && cola[i]!=null) return  i;
        }
        return -1;
    }
    public boolean swapEnds(){
        if (fondo<2) {return false;}
        else {
            Object temp = cola[fondo-1];
            Object temp2 = cola[0];
            cola[fondo-1] = temp2;
            cola[0] = temp;
            if (temp2 == cola[fondo-1] && temp == cola[0]) return true;
            else return false;
        }
    }
    public Iterator reverseIndexIterator(){
        return new ReverseIndexIterator();


    }
    
    @Override
    public Iterator<E> iterator() 
    {
        Iterator<E> it = new Iterator<E>() {
            private int punteroactual= 0;
            @Override
            public boolean hasNext() {
                if (punteroactual<fondo) return true;
                return false;
            }

            @Override
            public E next() {
                if (cola[punteroactual]!=null){
                    punteroactual++;
                    return (E)cola[punteroactual-1];
                }
                return null;
            }
            @Override
            public void remove(){throw new UnsupportedOperationException("No esta implementado.");}
        };
        return it;
    }
    private class ReverseIndexIterator implements Iterator<E> {
            private int punteroactual= fondo-1;
            @Override
            public boolean hasNext() {
                if (punteroactual>-1) return true;
                return false;
            }

            @Override
            public E next() {
                if (cola[punteroactual]!=null){
                    punteroactual--;
                    return (E)cola[punteroactual+1];
                }
                return null;
            }
            @Override
            public void remove(){throw new UnsupportedOperationException("No esta implementado.");}
    }

    @Override
    public int size() 
    {
        return this.fondo;
    }


    @Override
    public boolean offer(E e) 
    {
        if (contains(e)) throw new NullPointerException("Elemento nulo");
        if (fondo+1 == capacidad) this.asegurarCapacidad(capacidad*2);
        if (fondo==0){
            cola[fondo]= e;
            fondo++;}
        else{fondo++;
            cola[fondo]= e;}
        return true;
    }

    @Override
    public E remove() 
    {
        if (this.cola[0]==null) throw new NoSuchElementException("La cola esta vacia.");
        Object elim = cola[0];
        fondo--;
        System.arraycopy(cola, 1, cola, 0, fondo);
        return (E) elim;
    }

    @Override
    public E poll() 
    {
        if (this.cola[0]==null) return null;
        Object elim = cola[0];
        fondo--;
        System.arraycopy(cola, 1, cola, 0, fondo);
        return (E) elim;
    }

    @Override
    public E element() 
    {
        if (this.cola[0]==null) throw new NoSuchElementException("La cola esta vacia.");
        Object elim = cola[0];
        return (E) elim;
    }

    @Override
    public E peek() 
    {
        if (this.cola[0]==null) return null;
        Object elim = cola[0];
        return (E) elim;
    }


    @Override
    protected Object clone() throws CloneNotSupportedException 
    {
        return super.clone();
        }

    public boolean equals(TSBQueueUniqueValues e)
    {
        if (this.fondo!=e.fondo) return false;
        for (int i=0;i<this.fondo;i++){
            if (this.cola[i]!=e.cola[i]) return false;
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        return super.hashCode();
    }
    public String toString()
    {
        StringBuilder buff = new StringBuilder();
        buff.append('{');
        for (int i=0; i<fondo; i++)
        {
            buff.append(cola[i]);
            if(i < fondo-1)
            {
                buff.append(", ");
            }
        }
        buff.append('}');
        return buff.toString();
    }
}
